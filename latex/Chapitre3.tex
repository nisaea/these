%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%									Chapitre 3												%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Identifier des séquences virales: une tâche difficile}
\label{chap:difficile}
%	\citationChap{
%		% Citation
%	}{Auteur}
	\minitoc
	\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Début du chapitre

\begin{introbox}
	Ce chapitre porte sur la difficulté liée à l'identification de séquences virales au sein de données métagénomiques. Sont d'abord détaillées les raisons théoriques de cette difficulté, du point de vue de la nature des données manipulées et des spécificités propres au monde viral, puis sont présentés les résultats de l'estimation \textit{in silico} de cette difficulté, qui a fait l'objet d'une publication dans le journal \href{http://journal.frontiersin.org/journal/microbiology}{Frontiers in Microbiology} au cours de cette thèse (\citet{Soueidan2014}).
\end{introbox}



\section{Données de référence}
\label{sec:viralrefdata}

	\subsection{Propreté et pertinence des données}
	\label{sec:proprete}
	
\paragraph{Les bases de données publiques sont alimentées en permanence par la communauté scientifique} (cf. \ref{sec:genref}). Cela permet l'assurance d'un accès aux données correspondant aux travaux de recherche les plus récents, ainsi que des bases de données qui reflètent l'évolution des connaissances dans leur ensemble. Néanmoins, l'afflux grandissant de données (cf. Fig. \ref{fig:genbankgrowth}) pose de nombreux problèmes en terme de vérification de la validité des métadonnées et de la maintenance des différentes bases.

Par conséquent, les bases de données synchronisées par l'INSDC (cf. Tab. \ref{tab:INSDC}) ne sont pas en mesure d'assurer une non-redondance des données, ni une vérification manuelle des métadonnées. Même RefSeq (\url{http://www.ncbi.nlm.nih.gov/refseq/about/}), une base de données locale vérifiée manuellement et réputée pour sa fiabilité, ne peut retenir la publication des données non vérifiées et signale le statut de chaque entrée dans les commentaires qui lui sont associés. Ces données publiques sont donc bruitées par nature (cf. \ref{sec:donnees}), mais également du fait du système permettant leur mises en commun.

\paragraph{Les taxonomies de références ne sont pas des arbres complets ni des arbres parfaits.} Un arbre complet est un arbre dont toutes les feuilles sont sur le même niveau. Or, même si la plupart des branches ont des espèces pour feuilles, il peut arriver qu'il y ait des niveaux supplémentaires comme des sous-espèces ou des souches.

Un arbre est parfait si tous ses niveaux sont remplis, mais ce n'est pas le cas ici.  En effet, même si un ensemble de niveaux hiérarchiques standards est généralement respecté (cf. Fig. \ref{fig:phylogeny}), l'ensemble du vivant est bien trop hétérogène pour que de telles contraintes soient pertinentes dans tous les cas (cf. \ref{sec:espece}). Ainsi, lorsque l'on recherche la phylogénie complète de plusieurs espèces dans la phylogénie de référence du NCBI, il est possible d'obtenir un nombre très variable de niveaux phylétiques.

Par exemple, la bactérie modèle \textit{Escherichia coli} (cf. Tab. \ref{tab:ecoli}) ne possède pas de règne, alors qu'à l'inverse, la plante modèle \textit{Arabidopsis thaliana} (cf. Tab. \ref{tab:athal}) possède tous les niveaux standards auxquels ont été rajoutés de nombreux niveaux supplémentaires tels que la sous-classe et la tribu, ainsi que plusieurs niveaux ne possédant pas de dénomination.

Par conséquent, il est difficile de comparer l'ensemble du vivant sur un niveau phylétique donné, non seulement parce qu'ils ne sont pas toujours tous présent d'une espèce à l'autre, mais aussi parce qu'ils ne représentent pas systématiquement le même niveau de granularité, à plus forte raison lorsque les espèces sont éloignées les unes des autres. L'utilisation de ces bases de données est donc délicate et nécessite de prendre en compte l'ensemble des cas de figure et les incertitudes inhérentes à la systématique.



\begin{tableth}
	\centering
	\caption[Phylogénie: \textit{E. coli}]{Phylogénie complète de la bacterie \textit{Escherichia coli} selon la base de données \textit{taxonomy} (\url{http://www.ncbi.nlm.nih.gov/taxonomy}) du NCBI. Les niveaux phylétiques notés \textit{N/R} ne possèdent pas de nom (no rank).}
	\label{tab:ecoli}
	\begin{tabular}{|l|l|}
		\hline \rule[-1.2ex]{0pt}{4ex}
		\textbf{Niveau phylétique} & \textbf{Libellé}       \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Organismes cellulaires \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Domaine                    & Bactéries              \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Embranchement              & Protéobactéries        \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Classe                     & Gammaprotéobactéries   \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Ordre                      & Entérobacteriales      \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Famille                    & Entérobactériacées     \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Genre                      & Escherichia            \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Espèce                     & Coli                   \\ \hline
	\end{tabular}
\end{tableth}

\begin{tableth}
	\centering
	\caption[Phylogénie: \textit{A. thaliana}]{Phylogénie complète de l'Arabette des Dames, ou \textit{Arabidopsis thaliana} selon la base de données \textit{taxonomy} (\url{http://www.ncbi.nlm.nih.gov/taxonomy}) du NCBI. Les niveaux phylétiques notés \textit{N/R} ne possèdent pas de nom (no rank).}
	\label{tab:athal}
	\begin{tabular}{|l|l|}
		\hline \rule[-1.2ex]{0pt}{4ex}
		\textbf{Niveau phylétique} & \textbf{Libellé}       \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Organismes cellulaires \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Domaine                    & Eukaryotes             \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Règne                      & Chlorobiontes          \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Embranchement              & Streptophytes          \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Streptophytina         \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Embryophytes           \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Trachéophytes          \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Euphyllophytes         \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Spermatophytes         \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Magnoliophytes         \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Mésangiospermes        \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Eudicotylédones        \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Gunneridées            \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Pentapétalées          \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Sous-classe                & Rosidées               \\ \hline \rule[-1.2ex]{0pt}{4ex}
		N/R                        & Malvidées              \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Ordre                      & Brassicales            \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Famille                    & Brassicacées           \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Tribu                      & Camélinées             \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Genre                      & Arabidopsis            \\ \hline \rule[-1.2ex]{0pt}{4ex}
		Espèce                     & Thaliana               \\ \hline
	\end{tabular}
\end{tableth}

\paragraph{Il existe un très fort biais d'étude vers l'homme.} En effet, les espèces représentant un intérêt industriel ou médical sont particulièrement bien représentées dans les bases de données de référence et déséquilibrent fortement les quantités de données entre les différents clades.

\begin{figureth}
	\includegraphics[width=0.8\linewidth]{stack.png}
	\caption[Données RefSeq par domaine]{Répartition par domaine des données de la base RefSeq (\url{http://www.ncbi.nlm.nih.gov/refseq}) du NCBI. L'ensemble de ces données représente 7 932 410 séquences comprenant $474.10^{9}$ caractères au total. \textit{Source: NCBI, RefSeq release 08/09/2014}}
	\label{fig:refseq}
\end{figureth}

Ainsi, les eukaryotes sont particulièrement bien représentés car ils comprennent l'homme et la plupart des espèces utilisées dans l'industrie agroalimentaire (e.g. levures, bétail, poissons, cultures), de même que certaines espèces responsables d'épidémies actuelles (e.g. moustiques vecteurs de la malaria) (cf. Fig. \ref{fig:refseq}). Les bactéries également sont particulièrement bien étudiées, non seulement parce qu'elles comprennent de nombreux pathogènes, mais également pour l'intérêt industriel que représentent certaines espèces (e.g. traitement de la pierre, transformation de produits alimentaires, traitement des déchets, production de molécules dans l'industrie pharmaceutique).

À l'inverse, les archées sont très mal connues, si ce n'est pour la capacité de certaines espèces à vivre dans des milieux extrêmes (e.g. sources chaudes, lacs salés). Leur découverte est en effet très récente (\citet{Woese1977}, \citet{Balch1977}). Elles sont néanmoins présentes dans de nombreux biotopes, mais ne représentent que peu d'intérêt hors du contexte académique, car il n'existe pas actuellement d'exemple d'archée pathogène connu. Elles sont pour la plupart difficiles à cultiver en laboratoire, à l'instar de la plupart des microorganismes, mais ne bénéficient pas encore du recul nécessaire au développement de méthodes de cultures aussi spécifiques que celles dont bénéficient actuellement l'étude des bactéries.

\paragraph{Les virus souffrent également d'un fort biais anthropocentrique.} Nous avons vu que l'ensemble des virus connus sur lesquels nous disposons de données publiques est une sous-estimation dramatique de l'ensemble des espèces virales (cf. \ref{sec:virologie}), mais à cela s'ajoute un fort biais d'étude en faveur des pathogènes humains, ainsi que les agents responsables de pertes économiques. À titre d'exemple, une recherche sur le virus de la grippe dans la base de données GenBank (cf. \ref{sec:genref}) offre 304 900 résultats tandis que le Clavavirus, un virus de classe I (cf. Fig. \ref{fig:baltimore}) affectant \textit{Aeropyrum pernix}, une archée vivant obligatoirement en milieu salé à des températures proches de 100°C (\citet{SAKO1996}), ne bénéficie que d'une unique entrée (oct. 2015).

Ces disparités sont d'autant plus difficiles à surmonter que les virus étant des parasites obligatoires, les contraintes de culture sont nécessairement nombreuses. En effet, si l'hôte est lui-même difficile à isoler, ou si le titrage intracellulaire du virus est faible et qu'il ne possède pas de phase extracellulaire, comme c'est le cas des virus à mode de vie persistant (cf. \ref{sec:virus}), leur étude représente un défi dont la difficulté impacte directement leur représentation dans les bases de données publiques.


%- Hypervariabilité -> phylogénie arbitraire basée principalement sur la structure générale gérée de manière indépendante par l'ICTV

% récup intro

%- Assemblage de génomes: objectif VS réalité en structure et maintenance des bases de données



	\subsection{Génomes viraux complets: une denrée rare}
	
\paragraph{Les génomes viraux complets sont difficiles à obtenir.} Nous avons déjà vu plusieurs raisons à cela: leur culture souffre de nombreuses contraintes, et leurs modes de vie ne permettent pas toujours leur visibilité avec les méthodes actuelles, ni de justifier l'étude approfondie d'espèces ne présentant pas d'intérêt médical ou industriel.

Fort heureusement, le développement de la métagénomique apporte un début de solution à la difficulté de détection et d'identification de nouveaux virus. En revanche, le manque de génomes viraux de référence représente un obstacle majeur au développement de solutions robustes, car la détection de génomes en métagénomique repose massivement sur la comparaison des séquences présentes dans un échantillon aux génomes déjà connus et identifiés. Ainsi, les génomes viraux inconnus d'espèces non cultivables qui ne peuvent être comparées aux espèces de référence (car elles en sont trop éloignées) restent un défi majeur pour l'effort systématique actuel, et créent des lacunes importantes dans l'ensemble des connaissances actuelles en virologie.

Il s'agit d'un problème qui s'auto-alimente en permanence et ne pourra être surmonté qu'au fur et à mesure des découvertes de nouvelles espèces virales, tant à travers l'amélioration des méthodes de détection en métagénomique que par l'effort continu de développement de nouvelles méthodes de culture et d'analyse.

\paragraph{Ainsi, la quantité de génomes viraux reste mineure par rapport au vivant.} Il s'agit d'une contrainte qui, parce qu'elle ne bénéficie pas de solution immédiate, doit être soigneusement prise en considération tant lors du développement de nouvelles méthodes d'assignation taxonomique que lors de l'analyse des résultats obtenus par les méthodes existantes.

%- compétude des génomes et difficulté de la tâche lorsqu'il y a absence de génome proche donc pas de référence ou trop de mutations dans tous les sens

%- Résultat: données en quantité ridicule par rapport au vivant

\section{Difficultés structurelles propres aux virus}
\label{sec:virusdiff}

	\subsection{Absence de gène ubiquitaire}

\paragraph{Chez le vivant, les gènes ubiquitaires offrent des points de repère fiables.} Ils permettent de comparer entre elles même les espèces les plus éloignées (cf. \ref{sec:homologie}). Il n'est donc pas rare de découvrir de nouvelles souches ou espèces vivantes grâce à des outils d'alignement de séquences parmi des jeux de données métagénomiques, et de les replacer sur l'arbre phylogénétique du vivant en calculant leur distance évolutive par rapport aux espèces connues.

\paragraph{En revanche, les virus ne possèdent pas de gène ubiquitaire.} Les outils de base permettant leur reproduction appartiennent à l'hôte qu'ils infectent (cf. \ref{sec:virus}). Par conséquent, il n'est pas possible de faire une recherche exhaustive de toutes les espèces virales en présence dans un échantillon à partir d'un ensemble de gènes marqueurs. En effet, rien n'indique que toutes les espèces virales présentes partagent au moins un gène homologue aux gènes viraux connus.

Il est néanmoins possible d'effectuer ce type de recherche au sein de certaines familles spécifiques. Par exemple, les rétrovirus (classe VI, cf. Fig. \ref{fig:baltimore}) possèdent tous la particularité de se répliquer grâce à une étape de transcription inverse. Par conséquent, comme l'hôte ne possède pas de transcriptase inverse, il est possible de les identifier grâce à ce gène.

	\subsection{Ambiguïtés entre virus et hôte}
	\label{sec:ambiguite}

\paragraph{Certains génomes viraux s'insèrent dans les chromosomes de l'hôte.} En effet, nous avons vu que certains virus possèdent un mode de vie endogène (cf. \ref{sec:virus}). Ils sont donc présent physiquement au sein des chromosomes lorsque le génome de leur hôte est séquencé et assemblé. Par conséquent, parmi les génomes de référence des espèces vivantes, de nombreuses séquences virales sont présentes et constituent une source de bruit non négligeable. Les rétrovirus ont longtemps été considérés comme les seuls capables d'acquérir un mode de vie endogène, mais il a été récemment montré que d'autres familles virales en avaient la capacité (\citet{Horie2010}), ce qui suppose que le bruit induit affecteraient plus d'espèces que ce qu'il était autrefois admis. En effet, une étude d'autocorrélation de certaines méthodes par composition montrent que les régions correspondant à des prophages, virus endogènes bactériens, présentent une composition très différente du génome qu'ils occupent (\cite{Bohlin2008}).

\paragraph{Les virus échangent parfois des fragments de séquences avec d'autres organismes.} Il s'agit du phénomène de \textit{recombinaison} (\citet{Griffiths1999}) qui se produit lorsque deux molécules d'acide nucléique se trouvant à proximité l'une de l'autre échangent une partie de leur matériel (cf. Fig. \ref{fig:recombinaison}). Dans la plupart des cas, ces  événements nécessitent une homologie de séquence entre les deux molécules, mais les recombinaisons dites illégitimes, ne nécessitant pas ou peu d'homologie, existent et sont sources de divers réarrangements (e.g. délétions, insertions, translocations, multiplication du nombre de copies d'un gène, intégration virale).

\begin{figureth}
	\includegraphics[width=0.4\linewidth]{recombinaison.png}
	\caption[Recombinaison génétique]{Recombinaison génétique: réarrangement entre deux molécules d'acides nucléiques (ARN ou ADN) différentes (M, F), créant de nouvelles combinaisons génétiques (C1, C2). \textit{Source: Wikimedia Commons}}
	\label{fig:recombinaison}
\end{figureth}

Ce phénomène est un des éléments majeurs dans les mécanismes de l'évolution virale. Il est non seulement source de variabilité au sein d'une même espèce lorsque la recombinaison s'effectue entre deux particules virales, mais des échanges de matériel entre virus et hôte se produisent également (\citet{Filee2008}). Il existe même des cas dans lesquels des virus infectant les eukaryotes ont acquis de nombreux gènes bactériens (\citet{Filee2013}).


	\subsection{Hypervariabilité des génomes viraux}
	
\paragraph{Les événements de recombinaison sont particulièrement courant chez les virus.} Comme nous l'avons vu ci-dessus, les recombinaisons virales sont multiples et participent activement à l'évolution des espèces, mais elles sont surtout particulièrement fréquentes comparées à ce qu'il peut être observé chez le vivant (\citet{Onafuwa-Nuga2009}, \citet{Sztuba-Solinska2011}, \citet{Lukashev2010}, \citet{Martin2011}).

\paragraph{Les taux de mutation sont également extrêmement élevés} (\citet{Drake1999}, cf. Fig. \ref{fig:recombinaison}). En effet, les polymérases virales sont beaucoup plus sujettes aux erreurs de réplication que les polymérases cellulaires, surtout chez les polymérases ARN (\citet{Duffy2008}). À cela s'ajoute le fait que les mécanismes de réparation cellulaires sont souvent incompatibles avec les structures virales. La plupart de ces erreurs restent donc non corrigées. Le plus souvent, les génomes produits sont déficients, mais entre le grand nombre de particules virales et le phénomène de sélection naturelle, l'apparition de nouveaux variants viables est extrêmement fréquente et rapide (\citet{Lauring2013}, \citet{Paff2014}).

\begin{figureth}
	\includegraphics[width=0.6\linewidth]{mutrate.jpg}
	\caption[Taux de mutation]{Taux de mutation de différents types d'entités par rapport à la taille de leur génome. \textit{Source: (\citet{Gago2009})}}
	\label{fig:mutrate}
\end{figureth}

%- Illustration avec la grippe? ça parle à tout le monde ça

\section{Estimation in silico de la difficulté}
	
\paragraph{L'assignation taxonomique est une tâche à multiples dimensions.} Elle répond à un besoin simple: caractériser un échantillon en évaluant l'identité et la diversité des espèces en présence (cf. \ref{sec:metagenomique}). Mais la définition et l'application des différents protocoles permettant de l'effectuer peut varier grandement en fonction des questions de départ qui motivent sa mise en œuvre (cf. \ref{sec:assignatax}), et les contraintes associées ne sont pas les mêmes.

Comme nous venons de le voir, différents problèmes peuvent entraver l'identification des séquences virales dans un jeu de données métagénomiques. L'absence de gène ubiquitaire, les ambiguïtés entre virus et hôte, ainsi que l'hypervariabilité des géomes viraux sont autant de sources d'erreur qui soulignent la nécessité d'apporter des solutions pour minimiser la fréquence et l'importance desdites erreurs.

Dans ce contexte, il est important de pouvoir quantifier la difficulté de la tâche. C'est dans cet objectif qu'ont été menés les travaux suivants, publiés dans le journal \href{http://journal.frontiersin.org/journal/microbiology}{Frontiers in Microbiology} (\citet{Soueidan2014})

	\subsection{Pourquoi la classification par règne est-elle difficile?}
	\label{sec:why}

Alors que les méthodes spécifiques à la classification détaillée d'échantillons métagénomiques bénéficient de nombreuses avancées offrant de bonnes performances (cf. \ref{sec:detaillee}), les méthodes utilisables pour la classification par règne peinent à offrir des résultats fiables sur l'ensemble des espèces (cf. \ref{sec:aligndisc}, \ref{sec:compodisc}). Cela soulève naturellement la question des raisons d'un tel fossé. Puisque la caractérisation d'échantillons métagénomiques peut être formulée sous la forme d'une tâche d'apprentissage automatique supervisé, nous proposons ici l'utilisation de mesures de complexité des données afin de comparer la difficulté intrinsèque de chacune des approches dans le cadre de la classification d'échantillons métagénomiques.

Nous considérons ici trois tâches dont l'objectif est d'assigner une classe à chaque élément appartenant à un jeu de séquences. Les trois tâches décrites varient par la composition du jeu de séquences et par la portée des classes à assignées.

\begin{enumerate}
	\item{Pour un échantillon de séquences bactériennes donné, assigner chacune d'entre elles à un phylum (e.g. \textit{Proteobacteria}) ou à une classe (e.g. \textit{Gammaproteobacteria})}
	\item{Pour un échantillon de séquences virales donné, assigner chacune d'entre elles à une classe virale (e.g. dsDNA) ou à une famille (e.g. \textit{Plasmaviridae})}
	\item{Pour un échantillon de séquences donné, assigner chacune d'entre elles à un règne (e.g. bactérie, archée, eukaryote ou virus)}
\end{enumerate}

Les tâches (1) et (2) sont des problèmes de classification détaillée et miment des études métagénomiques ciblées, tandis que la tâche (3) représente une classification par règne et mime l'analyse d'échantillons complexes non ciblés (cf. \ref{sec:assignatax}). Comme nous nous intéressons à l'identification de nouvelles espèces dans de grands échantillons métagénomiques, nous avons adopté la représentation des séquences sous forme de vecteurs de fréquence de k-mers.

\paragraph{Nous avons analysé ces trois tâches de classification en utilisant une approche point par point de l'analyse de la complexité des données.} Dans l'apprentissage automatique supervisé, la performance d'un classifieur dépend non seulement de l'algorithme d'apprentissage (e.g. SVM ou Naïve Bayes) mais également des données d'entraînement. Même si les métriques globales récapitulent les performances générales d'un classifieur, elles ne permettent pas d'indiquer si des performances modérées sont le résultat d'un mauvais ajustement de paramètres, d'un biais d'échantillonnage dans les données d'apprentissage ou de la difficulté intrinsèque de la tâche de classification. Neanmoins, de récentes publications sur la mauvaise classification d'instances de données montrent que pour une tâche de classification donnée, certaines instances de données sont intrinsèquement difficiles à classifier et que leur présence est révélatrice de la difficulté globale (\cite{Smith2013}). La plupart des études concordent sur la difficulté que représentent les données aberrantes ou les instances de données appartenant à une classe minoritaire, mais Smith a démontré que des métriques simples permettent vraiment de quantifier la difficulté intrinsèque d'une instance de données. L'une de ces métriques est le \textit{k-Disagreeing Neighbors} (kDN), qui mesure, parmi les $k$ plus proches voisins d'une instance donnée, le nombre d'instances qui ne partagent pas la même classe. Smith a démontré que la mesure du kDN  est très positivement corrélée avec les erreurs de classification d'une instance donnée sur un large panel d'algorithmes d'apprentissage et de rééchantillonnages de données d'entraînement.

Afin de comparer la difficulté de classification des trois tâches, nous avons généré, à partir d'un sous-ensemble représentatif d'organismes séquencés dans GenBank (télé\-chargement de septembre 2014, 25624 BioProjects, 100\% des virus, archées et bactéries, 24 eukaryotes dont 18 plantes) 100 ensembles de 10000 fragments contigus choisis aléatoirement, d'une longueur moyenne de 500nt (correspondant à la taille moyenne de contigs métagénomiques afin de simuler une étape d'assemblage). Pour la tâche (1), seuls les génomes bactériens ont été considérés; pour la tâche (2), seuls les génomes viraux ont été considérés; pour la tâche (3), un mélange équilibré de virus, archées, bactéries et eukaryotes a été considéré. Chaque séquence a été représentée par un vecteur de fréquences d'apparition de 3-mers (i.e. le nombre de fois que chaque sous séquence de 3 nucléotides apparaît dans le contig) et nous avons défini la distance entre deux contigs comme étant la distance euclidienne entre leurs vecteurs à 64 ($4^{3}$) dimensions respectifs. Pour chaque contig, sa valeur de kDN est le nombre de contigs qui ne partagent pas la même classe que lui parmi ses 73 plus proches voisins. La difficulté de chaque classe correspondante est ensuite mesurée comme étant la valeur kDN médiane de tous les contigs de la classe donnée. Nous déterminons également si une valeur kDN médiane est significativement extrême (une valeur faible indique une classification facile, une valeur élevée correspond à une classe difficile), en estimant la distribution des valeurs kDN médianes sous l'hypothèse nulle de l'absence de relation entre les classes par des permutations aléatoires.

Dans la figure \ref{fig:needle1}, nous résumons la distribution des kDN par classe pour chacune des trois tâches. Les voisins sont déterminés  par rapport à la distance euclidienne dans l'espace des fréquences de 3-mers (cf. \ref{sec:why}). Par exemple, il y a plus de 6000 contigs archéens différents (barres rouges) qui ne possèdent pas un seul contig non-archéen parmi leurs 73 plus proches voisins (barre rouge correspondant à une valeur kDN de 0). La ligne pointillée représente la limite entre les contigs faciles à classifier correctement avec une majorité de voisins en accord avec eux (à gauche de la ligne) et ceux qui sont difficiles à classifier (à droite).

Le cadre du haut montre que pour la tâche de classification par règne, les contigs archéens et bactériens peuvent être facilement assignés à leur règnes respectifs, que cette classification est difficile pour les contigs eukaryotes, et encore plus difficile pour les virus. Lorsque la tâche est limitée aux bactéries seulement (cadres C1 et C2), la classification détaillée n'est pas difficile autant au niveau du phylum que de la classe. Pour les virus (cadres B1 et B2), la classification détaillée vers les classes virales (ssDNA, dsRNA, etc) est difficile, mais assigner une séquence virale au niveau de la famille est plus facile, même si ce n'est pas aussi aisé que pour les bactéries.



\begin{figureth}
	\includegraphics[width=\linewidth]{needle1.jpg}
	\caption[Distribution des kDN pour les trois tâches.]{Distribution des valeurs kDN par classe pour chacune des trois tâches. (A) Assignation de contigs de 500nt aux grands règnes (tâche 3); (B1,B2) Assignation de contigs de 500nt à une classe virale ou à une famille, respectivement (tâche 2); (C1,C2) Assignation de contigs bactériens de 500nt à un phylum ou à une classe, respectivelment (tâche 1). Chacun des 300000 contigs aléatoires est représenté par des vecteurs de fréquences de 3-mers. Les histogrammes indiquent combien de contigs (axe y) par classe (couleurs) possèdent un certain nombre de voisins (axe x) ne partageant pas leur propre classe, parmi les 73 plus proches. Seules les 4 classes les plus abondantes sont montrées pour (B1,C1); et 6 pour (B2,C2).}
	\label{fig:needle1}
\end{figureth}


En accord avec des travaux antérieurs (\cite{Mende2012}, \cite{Teeling2012}), nous avons vérifié que pour des contigs plus courts que 500nt, les distributions sont décalées vers la droite, ce qui correspond à une classification plus difficile. Inversement, pour des contigs plus longs que 500nt, les distributions sont décalées vers la gauche, ce qui correspond à une classification plus facile (cf. Fig. \ref{fig:supp1}).

\begin{figureth}
	\includegraphics[width=\linewidth]{supp1.png}
	\caption[Distribution des kDN avec de plus grands contigs]{Distribution des valeurs kDN par classe pour chacune des trois tâches, pour des contigs de 1000nt. Les cadres correspondent aux cadres de la Figure \ref{fig:needle1}.}
	\label{fig:supp1}
\end{figureth}

Il a été précédemment observé que les signatures virales de 3-mers  sont proches de celles de leurs hôtes (\cite{Pride2006}). Néanmoins, des preuves contredisant cette observation ont été également proposées, par exemple pour de grands virus (\cite{Mrazek2007}), ainsi que pour les virus des angiospermes (\cite{Adams2004}). Nous avons cherché à savoir si la difficulté de classification pouvait s'expliquer par la superposition des distributions de k-mers entre différents types d'hôtes et les virus qui les infectent. Dans ce but, nous avons échantillonné 4689 contigs depuis de grands groupes cellulaires (des génomes archéens, bactériens, végétaux et fongiques), ainsi que les virus connus comme les infectant. En utilisant une analyse en composante principale (PCA), nous avons projeté les vecteurs de fréquences de 3-mers de ces contigs sur deux dimensions. La Figure \ref{fig:needle2A} montre que les contigs viraux et cellulaires sont étalés uniformément sur ces deux dimensions, à l'exception des virus des plantes qui sont plus compacts. En utilisant une analyse de densité locale (cf. Fig. \ref{fig:needle2B}), nous avons observé (ellipses oranges) que les contigs des virus infectant les bactéries sont en effet proches de leurs hôtes (points 12 et 6, 13 et 7), mais qu'ils sont également proches des contigs archéens (points 13 et 3, 2 et 12). D'autre part, les virus des archées ne sont pas proches de leurs hôtes, tandis que les virus des plantes sont plus proches des bactéries et des archées que de leurs propres hôtes.

\begin{figureth}
	\includegraphics[width=\linewidth]{needle2A.png}
	\caption[Projection 2D des fréquences de 3-mers]{Projection 2D des fréquences de 3-mers pour les contigs cellulaires et viraux. Les 2 premières dimensions de la réduction par PCA des 28134 contigs (points) d'une longueur moyenne de 500nt representées par des vecteurs de fréquences de 3-mers; échantillonnées en parts égales depuis des génomes venant des 3 grands règnes cellulaires (rangée du haut) et des grandes familles virales dont on sait qu'elles les infectent (rangée du bas). La dimension 1 (axe x) représente 30\% de la variance, la dimension 2 (axe y) 8\% de la variance. Pour chaque sous-cadre, l'estimation par noyau sur ces deux dimensions est représentée par des lignes de contour rouges et les maximums de densité locaux sont indiqués dans les pastilles blanches.}
	\label{fig:needle2A}
\end{figureth}

\begin{figureth}
	\includegraphics[width=\linewidth]{needle2B-c.png}
	\caption[Projection 2D des fréquences de 3-mers, vue rapprochée]{Vue rapprochée de la Fig. \ref{fig:needle2A} avec tous les maximums de densité locaux. Les composantes principales ont été calculées une fois pour la totalité des contigs de tous les génomes à la fois. Les positions, coordonnées et axes de tous les cadres sont comparables.}
	\label{fig:needle2B}
\end{figureth}

	\subsection{Discussion}

Distinguer les séquences virales et cellulaires dans le cadre d'une étude environnementale non-ciblée reste un problème de classification sans solution à l'heure actuelle, particulièrement pour les espèces virales inconnues. Nous avons montré qu'une des raisons majeures pour lesquelles ce problème n'a toujours pas trouvé de solution satisfaisante est sa difficulté calculatoire intrinsèque. Elle réside dans le fait que les distributions de k-mers des séquences virales se superposent sans distinction avec celles des séquences cellulaires. Cela doit être mis en opposition avec la facilité relative avec laquelle cette tâche s'effectue chez les archées et les bactéries et qui explique certainement le succès des études portant sur l'assignation taxonomique de communautés bactériennes. La difficulté liée à la classification des séquences virales va s'alléger au fur et à mesure que les bases de données publiques de séquences génomiques s'enrichissent de données virales, mais cela ne résoudra pas suffisamment le problème de la découverte de nouvelles espèces.

Nous pensons sincèrement qu'un choix minutieux des méthodes informatiques utilisées, ainsi que des efforts supplémentaires de recherche dans cette direction sont les clefs du progrès dans ce domaine. En l'état actuel des connaissances, nous recommandons l'adoption d'une stratégie d'assemblage à l'état de contig, combinée avec une analyse basée sur des fréquences de k-mers, pour l'identification des séquences virales dans des échantillons métagénomiques. En ce qui concerne le développement de nouvelles méthodes, l'allègement de la rigueur de l'indexation de longs k-mers semble être une piste prometteuse (cf. \ref{sec:aligndisc}).
