%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%									Chapitre 2												%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{État de l'art}
\label{chap:edla}
%	\citationChap{
%		Citation
%	}{Auteur}
	\minitoc
	\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Début du chapitre

\begin{introbox}
	Ce chapitre offre une définition détaillé de la problématique traitée et des différentes tâches de classification qui lui sont associées, ainsi qu'une présentation des méthodes disponibles à l'heure actuelle.
\end{introbox}

\section{Cadre théorique}

	\subsection{Définition du problème: Assignation taxonomique}
	\label{sec:assignatax}

%		\subsubsection{Assignation taxonomique}
		

Nous nous intéressons ici à l'application de techniques d'apprentissage automatique à l'identification de séquences génomiques inconnues. Il s'agit d'un problème de classification supervisée de chaînes de caractères (cf. \ref{sec:classif}), dans lequel les objets manipulés sont la représentation de séquences génomiques sous forme de chaînes composées d'un alphabet de quatre lettres (cf. \ref{sec:donnees}), et dont les classes utilisées sont des taxons (cf. \ref{sec:homologie}).

\paragraph{Seuls seront considérés ici les classifieurs multiclasses.} En effet, les classifieurs binaires (e.g. FACS, \citet{Stranneheim2010}) sont plus adaptés à la décontamination d'échantillons qu'à l'identification de séquences. Nous explorerons ici les différents types de méthodes utilisés par ces classifieurs.

Un certain nombre de méthodes bioinformatiques effectuent de manière efficace ce type de classification sur des séquences issues d'échantillons contenant principalement des espèces connues. Elles peuvent être organisées grossièrement en deux catégories majeures:

\begin{itemize}
	\item{Les méthodes par similarité (cf. \ref{sec:class.similarite})}
%	\item{Les méthodes par phylogénie (cf. \ref{sec:class.phylogenie})}
	\item{Les méthodes par composition (cf. \ref{sec:class.composition})}
\end{itemize}


%- besoin de données sur les espèces connues, calcul de distance par rapport à ce qu'on connaît: même principe (plus il y a de mutations, plus on est loin du clade), calcul d'un score (distance) par clade pour voir de qui on se rapproche le plus

%- On utilise la plupart du temps les mêmes gènes chez le vivant que ceux qui ont permis la construction du tree of life (16S marche du tonnerre chez les bact par ex)

%- Pour l'identification de nouvelles espèces: seuils?
		
%		\subsubsection{Automatisation du processus}
%		\label{sec:auto}
		
%- Pour de grands volumes de données, il est possible de calculer des distances entre leurs séquences et ainsi utiliser l'outil informatique pour automatiser

%- Utilisation des données (séquences, taxonomie) contenues dans les banques publiques

%- Classification des données nouvelles après assemblage car sinon reads trop courts pour être informatifs: pb de la sous-spécificité des séquences trop courtes

\paragraph{Il est possible de décomposer la question de l'assignation taxonomique en fonction des problématiques de départ.} On définit ainsi deux grandes approches:

\begin{itemize}
	\item{\textbf{La classification par règne:}} Il s'agit ici de déterminer à quel grand règne appartient une séquence. En plus des grands règnes du vivant (archées, bactéries, eukaryotes), nous ajoutons une classe supplémentaire pour les virus. Dans ce contexte, il s'agit d'arriver à isoler les virus du reste des séquences afin de procéder ensuite à l'approche suivante, tout en minimisant les erreurs. Cette approche s'apparente à la décontamination des échantillons, qui a pour but de supprimer les séquences n'appartenant pas à l'organisme d'intérêt. En revanche, il s'agit ici de conserver les séquences appartenant à tous les organismes d'un grand taxon.
	\item{\textbf{La classification détaillée:}} On s'intéresse ici à la taxonomie des organismes desquels proviennent les séquences. On détermine leur position dans un arbre donné, jusqu'à une profondeur déterminée soit à l'avance, soit en fonction d'un indicateur de fiabilité des résultats. Cette approche a pour but d'identifier l'origine des séquences en présence.
\end{itemize}

La plupart des outils évoqués dans ce chapitre s'intéressent en priorité à la seconde approche, car la tâche est facilitée par la filtration et la purification des échantillons en amont du séquençage (cf. \ref{sec:sequencage}, \cite{Hall2014}). Mais la première approche, qui s'intéresse à des données complexes dans lesquelles sont présentes des séquences d'origines multiples, est particulièrement difficile (cf. \ref{sec:why}). Cette difficultée est d'autant plus importante lorsque des espèces virales inconnues y sont représentées, ce qui est souvent le cas (cf. \ref{sec:virologie}).

	\subsection{Classification par règne d'échantillons complexes}
	\label{sec:parregne}

La séparation par règne des séquences composant un jeu de données métagénomique complexe représente un défi particulièrement difficile au niveau informatique. La difficulté majeure réside dans les séquences virales inconnues pour lesquelles aucun homologue n'a encore été identifié, caractérisé et répertorié dans les bases de données publiques.

Leur identification est pourtant un objectif majeur pour les biologistes. En effet, certaines espèces virales ne peuvent pas être isolées par filtrage, comme en témoigne la découverte du Mimivirus dont la taille ($\approx400nm$, \citet{Xiao2009}) dépasse largement les plus petites bactéries ($<200nm$, \citet{Luef2015}). Néanmoins, leur découverte peut s'avérer essentielle pour l'identification de nouveaux pathogènes affectant l'homme, les plantes ou le bétail (\cite{Roossinck2012}, \cite{Lecuit2013}, cf. \ref{sec:virologie}).




	\subsection{Classification détaillée de communautés bactériennes et virales}
	\label{sec:detaillee}

De l'autre côté du spectre, le problème de la caractérisation détaillée de jeux de données produits par séquençage ciblé a subi d'importants progrès au cours de ces dernières années. Contrairement à l'analyse d'échantillons métagénomiques non sélectifs, et donc plus complexes, des méthodes efficaces ont été développées dans le cas où certaines communautées (bactériennes ou virales) sont sélectionnées expérimentalement. Il s'agit d'un moyen de contourner la difficulté de la classification par règne, sans néanmoins y apporter de solution.

Pour les communautés microbiennes, la solution la plus efficace est l'utilisation de gènes marqueurs, tels que l'ARNr 16S pour les prokaryotes et l'ARNr 18S pour les eukaryotes (fungi). Dans ce cas, seuls ces gènes sont séquencés et l'information génomique utilisée est très partielle. Cela simplifie l'analyse pour deux raisons. D'une part, le volume de données reste raisonnable (pour une analyse à haut débit), et d'autre part, la classification taxonomique de ces gènes marqueurs est disponibles dans des bases de données de référence spécialisées telles que RDP (\cite{Cole2009}) ou Greengenes (cf. \cite{DeSantis2006}). Les techniques par similarité de séquence combinées avec ces taxonomies de références sont extrêmement efficaces sur les distributions bactériennes connues (\cite{Bazinet2012}). En revanche, ce type d'analyse souffre d'un défaut majeur: il ne fournit pas de moyen fiable de quantifier les espèces identifiées (\cite{Roux2011}).

Tandis que cette approche est faisable pour des populations bactériennes, elle n'est pas applicable lors de l'analyse de communautés virales car l'absence de gènes marqueurs semblables (\cite{Edwards2005}) ne permet pas une comparaison directe de l'ensemble des espèces. L'étude de viromes se concentre sur la partie virale de l'échantillon et isole les particules virales encapsidées qui sont purifiées par filtration et (ultra)centrifugation. Cette approche à présent populaire réduit drastiquement la complexité des communautés, ce qui permet d'assembler systématiquement de plus grands contigs ($10^{3}$nt et plus), voire des génomes entiers dans le cas d'échantillons de faible complexité (\cite{Coetzee2010}, \cite{Minot2012}). En revanche, elle ne résoud pas vraiment le problème de la classification par règne, elle ne fait que l'éviter: suite à l'étape de purification, toutes les séquences sont généralement considérées "par définition" comme virales, jusqu'à preuve du contraire par des approches par homologie. De plus, cette stratégie n'est pas sans risque (pour plus de détails voir \cite{Fancello2012}). Par exemple, les particules purifiées peuvent contenir des fragments de génomes cellulaires au lieu du génome viral (cf. \ref{sec:virus}) à cause de la présence d'agents de transfert de gènes (\cite{Lang2007}) ou en conséquence de transduction généralisée (pour une revue voir \cite{Frost2005}). De plus, bien que filtrer les particules de plus de 220nm permet d'éviter la contamination par la plupart des cellules bactériennes, archéennes et eukaryotes, d'autres éléments contenant de l'ADN tels que des vésicules bactériennes (\cite{Biller2014}) peuvent être co-purifiées avec les virions. Une purification basée sur la filtration exclut également les plus gros virions, et ne permet pas d'obtenir une vision complète de la diversité virale. De plus, autant l'amplification LA (\cite{Duhaime2012}) que MDA ont leurs défauts (\citet{Kim2011}). Dans le cas de la première, la ligation des adaptateurs n'est possible que pour les virus à ADN double brin, et par conséquent les génomes viraux à ADN simple brin sont massivement absents dans l'échantillon. Dans le cas de la seconde, l'amplification est préférentiellement effectuée sur des virus à ADN simple brin circulaire au détriment des génomes à ADN double brin. Les conséquences de la présence de gènes cellulaires lors de l'analyse bioinformatique de données métagénomiques virales ont été décrites et des approches permettant de détecter leur présence ont été proposées (\cite{Roux2014}).

\paragraph{Malgré tout, l'étude des viromes a beneficié d'un large succès.} Contrairement aux communautés bactériennes, les méthodes basées sur l'alignement de séquences ne semblent pas être les plus adaptées pour la classification virale. En effet, tel qu'il a été mentionné dans \citet{Suttle2007}, même dans le cas de reads viraux relativement longs, la fréquence d'homologie entre ces reads et  les séquences protéiques au sein de la base de données Genbank n'est qu'autour de 30\%. L'idée est d'éviter les fortes contraintes séquentielles imposées par les méthodes d'alignement sur la similarité nucléotidique, et de capturer un signal de similarité globale basé sur la composition des séquences (k-mers). Les techniques basées sur la composition semblent offrir des résultats satisfaisants pour la classification taxonomique détaillée d'échantillons viraux filtrés (\cite{Yang2005}, \cite{Trifonov2010}).
			
\section{Classification par similarité}
\label{sec:class.similarite}

%\paragraph{Les méthodes par similarité peuvent elles-mêmes être divisées en deux catégories.} D'une part, les méthodes d'alignement de séquences, la plupart ayant pour but d'améliorer la précision de BLAST (cf. \ref{sec:alignement}), et d'autre part les méthodes par indexation.


	\subsection{Alignement de séquences}
	\label{sec:alignement}

Cette première catégorie de classifieurs se base sur la comparaison de séquences par les techniques d'alignement (cf. Fig. \ref{fig:align}). Chaque séquence inconnue est confrontée aux séquences présentes dans une banque de données contenant des séquences connues, et à chaque alignement est attribué un score calculé en fonction des caractéristiques prises en compte. Parmi ces dernières, on peut trouver les \textit{matches}, les \textit{mismatches} et plus rarement les \textit{gaps}, ces derniers pouvant être en outre pénalisés par leur longueur (cf. \ref{sec:homologie}).

\begin{itemize}
	\item{\textit{Match}:} Deux nucléotides identiques face à face au sein de l'alignement. Partant du postulat que l'histoire évolutive des séquences correspond le plus souvent à la suite d' événements possibles la plus parcimonieuse, il en est déduit que la position observée n'a pas subi de mutation depuis la divergence des deux séquences (e.g. les 7 premières positions dans la figure).
	\item{\textit{Mismatch}:} Deux nucléotides différents face à face. Partant du même principe, il est déduit que la position a subi un  événement de substitution (e.g. A en face de G à la 8e position).
	\item{\textit{Gap}:} Un ou plusieurs nucléotides ne trouvant pas de correspondance chez la séquence avec laquelle la séquence dont ils sont issus est alignée. Ces nucléotides supplémentaires peuvent être dus soit à un  événement d'insertion dans la séquence à laquelles ils appartiennent, soit à un  événement de délétion dans l'autre séquence de l'alignement (e.g. A sans corresondance en position 12).
\end{itemize}

Contrairement aux algorithmes d'alignement globaux utilisés lors de la construction d'arbres phylogénétiques, au cours de laquelle des séquences sont comparées sur toute leur longueur, ces classifieurs utilisent des algorithmes d'alignement local, qui permettent de trouver la région de plus forte similarité entre deux séquences, sans forcer l'alignement des régions peu ou non similaires. Ce type d'algorithme est bien mieux adapté car il s'agit ici de comparer des fragments courts à des données de référence généralement plus importantes, comme des génomes complets. En effet, là où un alignement global sera généralement utilisé pour vérifier une homologie entre deux séquences de longueur comparable, par exemple pour déterminer si deux gènes ont un lien de parenté, l'alignement local sera plus pertinent lors de la recherche d'appartenance d'un fragment de séquence à une référence connue. 

L'algorithme d'alignement local le plus célèbre est l'algorithme de Smith-Water\-man (\citet{Smith1981}). Il s'agit d'un algorithme optimal qui fournit l'alignement correspondant au meilleur score possible selon les valeurs de pondération choisies pour les matches, mismatches et gaps. Si son efficacité sur des petits jeux de données n'est plus à prouver, son exhaustivité implique néanmoins une quantité de calculs pouvant devenir problématique sur des données plus nombreuses (sa complexité étant de O(n²)).

\paragraph{Comme il n'est pas raisonnable d'énumérer tous les alignements possibles pour les volumes de données actuellement produits,} les logiciels d'alignement local font généralement usage de méthodes permettant de gagner du temps de calcul, en associant des heuristiques à la programmation dynamique (\citet{Durbin1998}).

C'est le cas de \textit{Basic Local Alignment Search Tool} (BLAST, \citet{Altschul1990}, cf. Fig. \ref{fig:BLAST}), une heuristique de recherche de similarité entre séquences biologiques, sur laquelle sont basées la plupart des méthodes de cette catégorie de classifieurs (\citet{Bazinet2012}). Elle a l'avantage d'offrir, en plus d'un système d'attribution d'un score par alignement, le calcul de la probabilité et l'espérance mathématique d'obtenir ledit score en alignant la séquence au hasard dans la banque. Ainsi, elle donne une mesure de la robustesse des résultats par rapport aux données de référence.

\begin{figureth}
	\includegraphics[width=0.8\linewidth]{blastscheme.png}
	\caption[BLAST]{Principe de fonctionnement de l'algorithme de BLAST. L'exemple illustré représente le cas d'alignement de séquences protéiques. \textit{Source: Université de Can Tho}}
	\label{fig:BLAST}. 
\end{figureth}

La grande majorité des méthodes de cette catégorie utilise BLAST comme méthode de calcul de similarité. La plupart, comme MARTA (\citet{Horton2010}), MEGAN (\citet{Huson2007}), MTR (\citet{Gori2011}), MG-RAST (\citet{Meyer2008}) ou encore Sort-ITEMS (\citet{MonzoorulHaque2009}), s'en sert de manière très directe, et chacune propose une façon originale d'en exploiter les résultats (cf. \ref{sec:LCA}). D'autres en proposent une utilisation moins directe. On peut ainsi citer MetaPhyler (\citet{Liu2011}), qui inclut une étape d'apprentissage au cours de laquelle l'outil va déterminer les régles selon lesquelles un résultat BLAST pourra offrir une fiabilité raisonnable, sur un jeu de séquences de référence composé de 31 gènes ubiquitaires. Ensuite, au cours de l'étape de classification, le meilleur hit de chaque séquence est interprété selon ces règles.

Alternativement, d'autres méthodes d'alignement sont utilisées pour la classification, comme les automates de Markov à états cachés (\textit{Hidden Markov Models}, HMM, \citet{Eddy2004}). Ce sont des modèles statistiques surtout très utilisés depuis les années 70 pour la reconnaissance vocale. Leur utilisation la plus connue en bio-informatique est la prédiction de gènes, mais ils sont aussi utilisés pour faire de l'alignement de séquences. Il s'agit d'automates permettant de détecter si une séquence présente des enchaînements de lettres similaires à celles qui ont permis leur construction (\citet{Durbin1998}). L'implémentation la plus courante est HMMER3 (\citet{Eddy2008}, cf. Fig. \ref{fig:HMMER}), notamment proposée comme méthode d'alignement alternative à BLAST dans CARMA3 (\citet{Krause2008}).

\begin{figureth}
	\includegraphics[width=0.8\linewidth]{HMMER.png}
	\caption[HMMER]{Exemple de diagramme d'états d'un automate de HMMER. Les états nommés "M" représentent les positions conservées de l'alignement d'apprentissage. Les états d'insertion "I" permettent l'ajout de nouvelles positons dans le cas d'une insertion, et les états de délétion "D" sont silencieux, et permettent de sauter certaines positions si elles ne sont pas conservées par rapport au modèle. \textit{Source: \citet{Johnson2006}}}
	\label{fig:HMMER}. 
\end{figureth}

		\subsection{L'algorithme LCA}
		\label{sec:LCA}

Afin d'exploiter les résultats de l'alignement d'une séquence aux séquences de référence d'une base de données, il faut déterminer une stratégie permettant de lui attribuer un taxon (cf. \ref{sec:homologie}). Pour cette étape, de nombreux outils utilisent l'algorithme LCA (Lowest Common Ancestor), décrit pour la première fois par \citet{Aho1973}. Le premier algorithme optimal fut décrit par \citet{Harel1984}, et il fut inclus dans un outil d'assignation taxonomique de séquences biologiques pour la première fois dans l'outil MEGAN (\citet{Huson2007}).

L'algorithme utilise la taxonomie du NCBI comme un arbre de classes (taxons). Il exploite les résultats d'alignement de la manière suivante:
\begin{itemize}
	\item Il liste tous les hits pour chaque séquence
	\item Pour chaque séquence $s$, il calcule l'ensemble $H$ de tous les taxons correspondant à la liste des hits de $s$
	\item Il cherche le nœud $v$ de plus bas niveau dans l'arbre qui enracine un sous-arbre comprenant la totalité de $H$ et assigne $s$ au taxon représenté par $v$
\end{itemize}

\begin{figureth}
	\includegraphics[width=0.15\linewidth]{lca.png}
	\caption[LCA]{Algorithme LCA: Exemple. Pour l'ensemble de séquences $s$ tous les hits se produisent sur l'ensemble de taxons $H=\left\{x;y\right\}$. Parmi tous les ancêtres communs de l'ensemble $H$ (en vert), celui de plus bas niveau est le LCA (en vert foncé). \textit{Source: Wikimedia Commons}}
	\label{fig:LCA}. 
\end{figureth}

Ainsi, le niveau du nœud correspondant à la classe attribuée à une séquence peut varier d'une séquence à l'autre. Plus une séquence est représentative de son taxon d'origine, plus le niveau du nœud sera bas, et plus le résultat sera précis. À l'inverse, certains gènes ubiquitaires, comme les gènes codant pour les ARNr, peuvent même se retrouver assignés à la racine de l'arbre.

\paragraph{Il existe des alternatives à l'algorithme LCA.} On peut notamment citer la méthode proposée par MTR (\textit{Multiple Taxonomic Ranks}, \citet{Gori2011}) qui utilise ici une approche de clustering dans le but de traiter des reads courts ($\sim$100nt) sans avoir recours à une étape d'assemblage. Pour chaque niveau taxonomique, les reads sont regroupés en clusters selon les taxons représentés par les résultats de leur alignement sur une base de données protéiques par BLASTx. Chaque cluster est ensuite associé à un taxon au niveau considéré, et les reads pour lesquels ce taxon n'est plus cohérent avec les taxons associés aux niveaux supérieurs sont assignés au taxon du niveau précédent et retirés du pool de reads pour les niveaux suivants.

		\subsection{Discussion}
		\label{sec:aligndisc}

Les méthodes par alignement souffrent de deux limitations majeures: une faible vitesse de calcul et une faible sensibilité (\cite{Bazinet2012}, \cite{Wood2014}). Récemment, de nouvelles solutions ont été apportées afin de dépasser ces limitations. Ces méthodes sont basées sur l'utilisation de longs k-mers et reposent sur le fait que si $k$ est suffisamment grand, les k-mers deviennent très spécifiques. Par conséquent, le principe de ces méthodes est l'indexation des bases de données de référence par ces longs k-mers. Il s'agit du principe de base de MegaBlast, un outil d'alignement généraliste de la suite d'outils BLAST, mais également de plusieurs méthodes spécifiques à l'assignation taxonomique  telles que LMAT (\cite{Ames2013a}), Kraken (\cite{Wood2014}) et CLARK (\cite{Ounit2015}). Le désavantage de ces approches est la sur-spécificité, qui rend problématique la classification de séquences provenant d'espèces inconnues.

Cette limitation peut être d'autant plus dramatique chez les virus, sachant leur très grande variabilité intraspécifique. Par exemple, les critères actuels de l'ICTV (cf. \ref{sec:espece}) tolèrent jusqu'à 28\% de divergence pour les gènes codant pour la poly\-mérase ou les protéines de la capside pour des individus de la même espèce dans la famille des \textit{Betaflexviridae} et un niveau de divergence similaire pour la totalité du génome chez les \textit{Potyviridae} (\cite{King2011}). Par conséquent, la faible qualité des alignements entre séquences homologues appartenant à des membres d'une même espèce peut rendre la détection de cette appartenance particulièrement difficile.

Malgré tout, plueieurs outils spécialisés pour les séquences virales ont vu le jour comme PASC (\cite{Bao2014,Bao2012}), qui utilise BLAST pour des alignements locaux et l'algorithme de Needleman-Wunsch pour des alignements globaux, ou plus récemment DEmARC (\cite{Lauber2012,Lauber2012a,Lauber2012b}) qui utilise une mesure de divergence basée sur l'estimation du maximum de vraisemblance sur un alignement multiple de gènes marqueurs. Ces outils ne sont utilisable qu'au sein de familles virales particulières qui partagent des gènes communs et une histoire évolutive et, par conséquent, répondent à une problématique bien différente de la nôtre. Ils sont cependant très utilisés dans le cadre de l'effort d'amélioration de la taxonomie virale, et dans le cadre de l'étude de variants d'espèces d'intérêt.

\paragraph{Les méthodes d'alignement de séquences} sont très dépendantes des relations d'homologie entre les séquences-requêtes et le contenu des bases de données auxquelles elles sont comparées. Pour cette raison, plus les distances évolutives sont grandes entre les deux, plus il va être difficile d'établir une classification fiable. Il est possible d'être confronté à cette limitation si le contenu de la base de données ne contient pas de génomes proches de la requête, si elle appartient par exemple à un organisme inconnu pour lequel aucun proche parent n'a encore fait l'objet d'un effort de séquençage. Sachant la difficulté à isoler de nombreux génomes (cf. \ref{sec:metagenomique}), ainsi que la grande variabilité de certaines espèces, en particulier chez les virus, il n'est pas surprenant d'observer de mauvaises performances sur certains échantillons. En effet, une grande partie des reads produits par des projets de séquençage \textit{de novo} d'espèces virales sont classifiés comme "inconnus" (\cite{Bzhalava2012},\cite{Bzhalava2013}).

%Pb: HMM: gros drops d'autocorrélation pour gènes ubiquitaires et transferts horizontaux comme les prophages


\paragraph{L'algorithme LCA} (cf. \ref{sec:LCA}) utilise le parcours d'un arbre taxonomique pour trouver le plus petit ancêtre commun entre les taxons présentant un hit pour une séquence donnée. Cette méthode part du postulat que la taxonomie est construite selon les principes de la cladistique de sa racine à ses feuilles, c'est à dire qu'il y a des liens de parenté mesurables entre les différents nœuds de l'arbre. La taxonomie virale (cf. \ref{sec:espece}) est un peu différente dans le fait qu'elle ne vérifie ces principes que de manière locale. Il y a donc une incompatibilité théorique fondamentale avec l'algorithme LCA dans le fait qu'il suppose l'existence d'un dernier ancêtre commun universel à la racine de l'arbre, que la taxonomie virale n'est à l'heure actuelle pas en mesure de fournir, ni même d'en affirmer l'existence.

Ce reproche est aussi applicable à MTR, mais s'y ajoutent également d'autres préoccupations. L'approche est intéressante si une étape d'assemblage n'est pas indispensable pour le reste de l'analyse des données, mais peut être handicapante si les reads ne sont plus accessibles. De plus, les auteurs comparent les deux méthodes sur des reads courts, mais ne précisent pas si l'avantage de MTR survit à une étape d'assemblage en amont de l'algorithme LCA.


\section{Classification par composition}
\label{sec:class.composition}

\paragraph{Une autre approche est basée sur l'analyse de la composition des séquences.} La simplicité de l'alphabet qui compose les séquences d'ADN peut surprendre, lorsqu'on sait la complexité de l'information qu'elles contiennent. Observer et comparer les différentes régions de l'ADN permet de mettre en évidence une grande hétérogénéité de composition avec, par exemple, des motifs isolés très conservés spécifiques à certaines fonctions biologiques (e.g. séquences promotrices et régulatrices) et de longues zones contenant des motifs répétés très variables pouvant jouer un rôle structurel crucial (e.g. régions centromériques et télomériques).

Il s'agit ici de comparer les fréquences d'apparition de certains motifs entre les séquences, sans tenir compte de leur ordre d'apparition sur les brins. Contrairement à l'approche par similarité, il ne s'agit pas d'aligner les séquences mais d'extraire, à partir de leur composition, des signatures spécifiques et comparables entre lesquelles il est possible de calculer des distances.

\paragraph{Cette approche se base sur la notion de \textit{biais compositionnel}.} Il a été démontré que la composition des séquences varie non seulement d'une région génomique à l'autre, mais également d'un taxon à l'autre, et que ces biais sont porteurs d'information sur l'histoire évolutive des espèces (\cite{Gautier2000,Mooers2000}). Ainsi, à l'époque où le séquençage NGS n'existait pas encore (cf. \ref{sec:sequencage}), il était recommandé d'utiliser la température de dénaturation de l'ADN, à partir de laquelle il est possible de calculer la proportion de G-C dans les séquences, pour la définition d'unités taxonomiques dans le cadre de la systématique bactérienne (\cite{Moore1987}).

En revanche, même s'il s'agit d'un outil de mesure qui a fait ses preuves en phylogénétique bactérienne, la proportion de G-C se montre insuffisante à plus grande échelle et ne suffit plus à caractériser les taxons de manière suffisamment spécifique (\cite{Foerstner2005}). Afin de remédier à ce problème, l'approche par composition étend le concept de biais compositionnel à des motifs plus spécifiques composés de plusieurs nucléotides.

De plus, contrairement aux méthodes d'alignement qui dépendent fortement de la conservation de la contiguité des segments homologues, les techniques utilisant la composition permettent de s'en affranchir et ainsi récupérer de la pertinence dans le cas de génomes très variables comme les génomes viraux (\citet{Vinga2003}).

\paragraph{Ces méthodes reposent sur la décomposition des séquences en fréquences d'apparition de k-mers courts} (non-uniques) et utilisent des techniques d'apprentissage automatique (e.g. SVM, kNN, Naïve Bayes, etc., cf. \ref{sec:machlearn}) afin d'entraîner un classifieur sur une base de données de référence. L'assignation taxonomique de séquences inconnues est ensuite prédite par l'application de ce modèle pré-entraîné. Ces méthodes sont théoriquement plus adaptées à la classification d'espèces inconnues, car les distributions de k-mers courts sont moins sensibles au surapprentissage. 

\paragraph{Il existe d'autres types d'approches sans alignement.} Il est notamment important de citer NVR (\cite{Yu2013}) car il s'agit d'un outil spécialisé pour les séquences virales. Il repose sur un système de vecteur htérogène contenant 12 variables calculées à partir de la séquence. On y trouve le nombre d'occurrence de chaque lettre, leur position moyenne au sein de la séquence, et un coefficient calculé en fonction des deux variables précédentes et de chaque position de la lettre courante. Il s'agit d'une approche très intéressante qui permet de raisonner très haut dans la taxonomie, mais qui ne fonctionne correctement que sur des génomes entiers. Elle ne répond donc pas à notre problématique mais est très utilisée pour tenter de compléter, prédire et corriger la taxonomie virale, et a déjà permis d'offrir une place à de nombreux génomes viraux dont la taxonomie était incomplète.

	\subsection{Distributions de k-mers et signatures}
	\label{sec:kmers}

Les k-mers sont des mots de taille $k$ lus sur une séquence à travers une fenêtre glissante que l'on déplace d'une base à chaque itération (cf. Fig. \ref{fig:kmersig}). L'alphabet $A=\left\{ "A", "C", "G", "T" \right\}$ utilisé dans les séquences génomiques étant de taille $4$, le nombre total de mots possibles est $4^k$.


\begin{figureth}
	\begin{subfigureth}{0.4\textwidth}
		\includegraphics[width=0.8\linewidth]{kmers}
		\caption{Décomposition d'une séquence en k-mers}
		\label{sub:Antibes}
	\end{subfigureth}
	\begin{subfigureth}{0.4\textwidth}
		\includegraphics[width=0.7\linewidth]{signature}
		\caption{Nombre d'occurrences de chaque k-mer}
		\label{sub:SaintJeannet}
	\end{subfigureth}
	\caption[Signature d'une séquence]{Extraction de la signature d'une séquence à partir de sa composition en k-mers. Nous prenons ici k=2 à titre d'exemple. (a) Tous les k-mers composant la séquence sont répertoriés avec une fenêtre glissante de taille k se déplaçant de 1 en 1. (b) Le nombre d'occurrences de chaque k-mer possible ($4^k$ possibilités) est stocké dans un tableau. Ce tableau constitue la signature de la séquence, et le nombre d'occurrences peut être normalisé par la taille. On obtient alors un tableau de fréquences.}		
	\label{fig:kmersig}
\end{figureth}

Des premiers travaux (\cite{Karlin1995}) se sont rapidement intéressés aux fréquences d'apparition de dinucléotides ($k=2$) afin de pallier au manque de spécificité des méthodes utilisant le contenu en G-C, et ont pu mettre en évidence une grande stabilité des fréquences des dinucléotides au sein d'un même génome, ainsi que d'importantes disparités entre différentes espèces (cf. Fig. \ref{fig:ecoliVScelegans}), confirmant ainsi l'intérêt d'utiliser des distributions de k-mers en tant que signatures spécifiques afin de différencier plusieurs espèces.

\begin{figureth}
	\includegraphics[width=0.6\linewidth]{ecoliVScelegans.png}
	\caption[Fréquences de dinucléotides chez \textit{E. coli} et \textit{C. elegans}]{Comparaison des fréquences relatives de certains dinucléotides entre \textit{E. coli} et \textit{C. elegans}. $\rho\textbf{*}_{XY}=\frac{f_{XY}}{f_{X}f_{Y}}$ où $f_{X}$ est la fréquence du nucléotide $X$ et $f_{XY}$ est la fréquence du dinucléotide $XY$ dans la séquence et son complément. \textit{Source: \cite{Karlin1995}}}
	\label{fig:ecoliVScelegans}
\end{figureth}

Un peu plus tard, grâce aux progrès de l'informatique offrant d'avantage de puissance de calcul et de stockage, plusieurs outils d'assignation taxonomique utilisant des k-mers courts (non uniques) ont vu le jour. Deux approches principales peuvent être distinguées:

\begin{itemize}
	\item \textbf{L'approche par génome entier:} Cette approche consiste à comparer les fréquences de k-mers d'une séquence à classifier avec celles des génomes de référence dans leur intégralité. La plupart de ces méthodes utilisent des modèles de Markov, comme Swaap PH (\cite{Pride2003}), ZOM/MCM (\cite{Bohlin2008}), Phymm/PhymmBL (\cite{Brady2009}) ainsi que MGTAXA (\url{http://andreyto.github.io/mgtaxa}) qui est directement inspiré de la méthode de Phymm.
	
	D'autres méthodes proposent différentes manières de calculer la distance entre la distribution de k-mers d'une séquence à classifier et celles des génomes de référence. La plus simple, proposée par \cite{Bohlin2008}, consiste à calculer le coefficient de pearson entre chaque vecteur-requête et chaque vecteur de référence.
	
	TACOA (\cite{Diaz2009}) propose des vecteurs de k-mers un peu plus complexes, contenant le ratio entre la fréquence d'apparition de chaque k-mer et sa fréquence attendue sachant le contenu en G-C de la séquence. Une fonction discriminante est ensuite calculée taxon par taxon afin de calculer un score d'appartenance pour chaque séquence.
	
	D'autres mesures de distance ont été explorées par \cite{Trifonov2010}, spécifiquement pour des données virales de classe V. Les auteurs ont notamment testé la distance de Manhattan, la distance euclidienne, le test du $\chi^2$, ainsi qu'une version symétrisée de la divergence de Kullback-Leibler, une mesure de distances basée sur l'entropie de Shannon
	
	\item \textbf{L'approche par rééchantillonage:} Il s'agit ici de rééchantilloner les génomes de référence afin d'obtenir des fragments de ces génomes ayant des tailles comparables aux séquences à classifier. C'est à partir de ces fragments que les vecteurs de fréquences de k-mers sont calculés. Dans ce contexte, chaque génome de référence est représenté par un ensemble de vecteurs de fréquences différents les uns des autres. Ces vecteurs peuvent être considérés comme des coordonnées dans un espace à $4^k$ dimensions, et cette approche se base sur l'idée selon laquelle les vecteurs appartenant à un même taxon seront rapprochés dans l'espace.
	
	PhyloPythia (\cite{McHardy2007}) utilise cette représentation afin d'entraîner une machine à vecteur de support linéaire sur de long fragments (>1000nt). D'autres algorithmes d'apprentissage automatiques ont été utilisés dans la suite d'outils RDP (\cite{Wang2007}) en utilisant des données d'ARNr comme gènes marqueurs: un classifieur naïf bayésien (RDP classifier) et l'algorithme des $k$ plus proches voisins (RDP SeqMatch).
	
	RAIphy (\cite{Nalbantoglu2011}) utilise une méthode différente. Tous les vecteurs de fréquences des fragemnts d'un même taxon sont utilisés afin de construire un unique vecteur, appelé Index d'Abondance Relative, qui contient, pour chaque k-mer, un score de représentation calculé avec une approche markovienne d'ordre 1. Ce score est positif si le k-mer est surreprésenté dans le taxon, et négatif s'il est sous-représenté. Pour chaque séquence à classifier, un score d'appartenance est calculé pour chaque taxon en faisant la somme de tous les scores de représentation de l'index du taxon, pondérée par les valeurs du vecteur de fréquences de k-mers de la séquence à classifier. Plus les fréquences d'apparition des k-mers de la séquence coïncideront avec l'abondance relative de ces mêmes k-mers au sein du taxon, plus le score d'appartenance sera élevé.
\end{itemize}

On peut également citer NBC (\cite{Rosen2011}), un outil à la limite entre alignement et composition utilisant un classifieur bayésien naïf, qui a la particularité de ne pas utiliser de génomes entiers ou de fragments rééchantillonnés, mais de reads issus de données réelles, d'une longueur moyenne de 230nt.

		\subsection{Discussion}
		\label{sec:compodisc}

\paragraph{Les méthodes par composition bénéficient depuis quelques années d'un certain succès.} En effet, le nombre d'outils ayant vu le jour est témoin d'un intérêt indéniable pour ce type d'approche. S'affranchir de la séquentialité des nucléotides n'est pas chose aisée compte tenu de l'importance historique des méthodes par alignement dans l'étude des relations d'homologie, mais ouvre des portes en terme de gestion des réarrangements au niveau moléculaire, à travers une souplesse que ne permettent pas les outils d'alignement.

En revanche, même ces techniques ne parviennent pas à classifier autour de 50\% des espèces qui ne sont pas représentées dans le jeu de données d'apprentissage (\cite{Nalbantoglu2011}). Cela est d'autant plus valable pour les séquences virales, dont la plupart ne parviennent pas à être assignées à un quelconque règne (\cite{Rosen2011}). En effet, pour les séquences virales (mais également eukaryotes), aucune des méthodes existantes ne produit une distribution taxonomique qui s'approche de la distribution attendue (\cite{Bazinet2012}).

\paragraph{Ces méthodes sont très sensibles.} De nombreux facteurs peuvent influencer leur efficacité de manière dramatique. Les paramètres utilisés par les algorithmes sont évidemment des acteurs importants, mais les données manipulées jouent également un rôle crucial. En effet, la longueur des k-mers, la taille des séquences, la présence ou non des organismes dans les données d'apprentissage ainsi que les espèces représentées peuvent faire grandement varier la qualité des résultats (\cite{Trifonov2010}).

De plus, les génomes ne sont pas homogènes. Cela pose problème pour les deux approches. En effet, une signature unique par génome entier ne reflète pas les variations locales et souffre ainsi d'une perte d'information importante parmi les données d'apprentissage. De la même manière, si la couverture génomique des fragments rééchantillonés sur leurs génomes d'origine n'est pas suffisante, il est possible que certaines variations locales disparaissent des données d'apprentissage et constituent un angle mort pour le classifieur. Ce phénomène est bien illustré par \cite{Bohlin2008} qui montre de grandes variations d'autocorrelation locales sur une fenêtre glissante de 5000 nucléotides. Ces variations mettent également en évidence des régions de très faible autocorrélation correspondant aux ARNr mais également à de nombreux prophages, virus endogènes bactériens.

\paragraph{Il s'agit néanmoins d'approches prometteuses.} Leur nature sensible ne font pas d'elles de bonnes candidates pour un outil universel mais montrent de bon résultats lorsqu'elles sont utilisées dans le cadre précis dans lequel elles ont été développées.

%\section{Classification hybride}



%\bibliographystyle{francaissc}
%\bibliography{Chapitre2/Biblio}